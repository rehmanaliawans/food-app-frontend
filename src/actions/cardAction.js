import axios from "axios";
import {
  ADD_CART_FAIL,
  ADD_CART_SUCCESS,
  ADD_ORDER_FAIL,
  ADD_ORDER_REQUEST,
  ADD_ORDER_SUCCESS,
  ALL_LIST_ORDER_FAIL,
  ALL_LIST_ORDER_REQUEST,
  ALL_LIST_ORDER_SUCCESS,
  CART_REMOVE,
  LIST_ORDER_FAIL,
  LIST_ORDER_REQUEST,
  LIST_ORDER_SUCCESS,
  ORDER_STATUS_FAIL,
  ORDER_STATUS_REQUEST,
  ORDER_STATUS_SUCCESS,
} from "../constants/cardConstant";
import axiosInstance from "../utils/axiosInstance";

export const removeCartItem = (id) => async (dispatch) => {
  try {
    dispatch({
      type: CART_REMOVE,
      id,
    });
  } catch (error) {
    console.log(error);
  }
};

export const addCartItem = (data) => async (dispatch) => {
  try {
    dispatch({
      type: ADD_CART_SUCCESS,
      data,
    });
  } catch (error) {
    console.log(error);
    const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message;
    dispatch({
      type: ADD_CART_FAIL,
      payload: message,
    });
  }
};

export const addOrder =
  (resturantid, price, status, mealList) => async (dispatch, getState) => {
    try {
      dispatch({ type: ADD_ORDER_REQUEST });
      const {
        userLogin: { userData },
        resutrantInfo: { resturant },
      } = getState();
      const id = userData._id;
      const name = userData.firstname;
      const config = {
        headers: {
          "Content-Type": "application/json",

          authorization: `Bearer ${userData.token}`,
        },
      };
      const { orderData } = await axiosInstance.post(
        `/api/order/add/${resturantid}`,
        {
          firstname: name,
          price,
          status,
          meals: {
            title: mealList.title,
            price: mealList.price,
          },
          user: id,
        },
        config,
      );
      dispatch({
        type: ADD_ORDER_SUCCESS,
        orderData,
      });
    } catch (error) {
      console.log("add order error", error);
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: ADD_ORDER_FAIL,
        payload: message,
      });
    }
  };

export const listOrderAction = (id) => async (dispatch, getState) => {
  try {
    dispatch({ type: LIST_ORDER_REQUEST });
    const {
      userLogin: { userData },
    } = getState();

    const config = {
      headers: {
        Authorization: `Bearer ${userData.token}`,
      },
    };
    const { data } = await axiosInstance.get(`/api/order/${id}`, config);

    dispatch({
      type: LIST_ORDER_SUCCESS,
      data,
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message;
    dispatch({
      type: LIST_ORDER_FAIL,
      payload: message,
    });
  }
};

export const listAllOrderAction = () => async (dispatch, getState) => {
  try {
    dispatch({ type: ALL_LIST_ORDER_REQUEST });
    const {
      resutrantInfo: { resturant },
      userLogin: { userData },
    } = getState();
    const config = {
      headers: {
        Authorization: `Bearer ${userData.token}`,
      },
    };

    let newRestIds = [];
    for (let index = 0; index < resturant.length; index++) {
      newRestIds[index] = resturant[index]._id;
    }

    const { data } = await axiosInstance.get(
      `/api/order`,
      { params: { newRestIds } },
      config,
    );

    dispatch({
      type: ALL_LIST_ORDER_SUCCESS,
      data,
    });
  } catch (error) {
    console.log(error);
    const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message;
    dispatch({
      type: ALL_LIST_ORDER_FAIL,
      payload: message,
    });
  }
};
export const changeOrderStatus = (id, status) => async (dispatch, getState) => {
  try {
    dispatch({ type: ORDER_STATUS_REQUEST });

    const {
      userLogin: { userData },
    } = getState();

    const config = {
      headers: {
        Authorization: `Bearer ${userData.token}`,
      },
    };
    const updateid = userData._id;
    const name = userData.firstname;

    await axiosInstance.put(
      `/api/order/order-status`,
      {
        id,
        status,
        updateid,
        name,
      },
      config,
    );

    dispatch({
      type: ORDER_STATUS_SUCCESS,
      id,
      status,
    });
  } catch (error) {
    console.log(error);
    dispatch({
      type: ORDER_STATUS_FAIL,
    });
  }
};
