import axios from "axios";
import {
  ITEM_CREATE_FAIL,
  ITEM_CREATE_REQUEST,
  ITEM_CREATE_SUCCESS,
  ITEM_DELETE_FAIL,
  ITEM_DELETE_REQUEST,
  ITEM_DELETE_SUCCESS,
  ITEM_LIST_FAIL,
  ITEM_LIST_REQUEST,
  ITEM_LIST_SUCCESS,
  ITEM_UPDATE_FAIL,
  ITEM_UPDATE_REQUEST,
  ITEM_UPDATE_SUCCESS,
} from "../constants/itemConstant";
import axiosInstance from "../utils/axiosInstance";

export const listItemAction = (id) => async (dispatch, getState) => {
  try {
    dispatch({ type: ITEM_LIST_REQUEST });
    const {
      userLogin: { userData },
    } = getState();

    const config = {
      headers: {
        Authorization: `Bearer ${userData.token}`,
      },
    };

    const { data } = await axiosInstance.get(`/api/item/${id}`, config);

    dispatch({
      type: ITEM_LIST_SUCCESS,
      payload: data,
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message;
    dispatch({
      type: ITEM_LIST_FAIL,
      payload: message,
    });
  }
};

export const createItemAction =
  (id, title, description, price) => async (dispatch, getState) => {
    try {
      dispatch({ type: ITEM_CREATE_REQUEST });
      const {
        userLogin: { userData },
      } = getState();

      const config = {
        headers: {
          "Content-Type": "application/json",

          Authorization: `Bearer ${userData.token}`,
        },
      };
      const { data } = await axiosInstance.post(
        `/api/item/create/${id}`,
        {
          title,
          description,
          price,
        },
        config,
      );
      dispatch({
        type: ITEM_CREATE_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: ITEM_CREATE_FAIL,
        payload: message,
      });
    }
  };

export const updateItemAction =
  (id, title, description, price) => async (dispatch, getState) => {
    try {
      dispatch({
        type: ITEM_UPDATE_REQUEST,
      });

      const {
        userLogin: { userData },
      } = getState();
      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${userData.token}`,
        },
      };

      const { data } = await axiosInstance.put(
        `/api/item/${id}`,
        { title, description, price },
        config,
      );

      dispatch({
        type: ITEM_UPDATE_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: ITEM_UPDATE_FAIL,
        payload: message,
      });
    }
  };

export const deleteItemAction = (id) => async (dispatch, getState) => {
  try {
    dispatch({
      type: ITEM_DELETE_REQUEST,
    });

    const {
      userLogin: { userData },
    } = getState();

    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${userData.token}`,
      },
    };

    const { data } = await axiosInstance.delete(`/api/item/${id}`, config);

    dispatch({
      type: ITEM_DELETE_SUCCESS,
      payload: data,
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message;
    dispatch({
      type: ITEM_DELETE_FAIL,
      payload: message,
    });
  }
};
