import axios from "axios";
import {
  ALL_RESTURANT_LIST_FAIL,
  ALL_RESTURANT_LIST_REQUEST,
  ALL_RESTURANT_LIST_SUCCESS,
  RESTURANT_CREATE_FAIL,
  RESTURANT_CREATE_REQUEST,
  RESTURANT_CREATE_SUCCESS,
  RESTURANT_DELETE_FAIL,
  RESTURANT_DELETE_REQUEST,
  RESTURANT_DELETE_SUCCESS,
  RESTURANT_LIST_FAIL,
  RESTURANT_LIST_REQUEST,
  RESTURANT_LIST_SUCCESS,
  RESTURANT_UPDATE_FAIL,
  RESTURANT_UPDATE_REQUEST,
  RESTURANT_UPDATE_SUCCESS,
} from "../constants/resturantConstant";
import axiosInstance from "../utils/axiosInstance";

export const listResturantAction = () => async (dispatch, getState) => {
  try {
    dispatch({ type: RESTURANT_LIST_REQUEST });
    const {
      userLogin: { userData },
    } = getState();
    console.log("user data", userData);
    const config = {
      headers: {
        Authorization: `Bearer ${userData.token}`,
      },
    };
    const { data } = await axiosInstance.get("/api/resturant", config);
    dispatch({
      type: RESTURANT_LIST_SUCCESS,
      payload: data,
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message;
    dispatch({
      type: RESTURANT_LIST_FAIL,
      payload: message,
    });
  }
};

export const listAllResturantsAction = () => async (dispatch, getState) => {
  try {
    dispatch({ type: ALL_RESTURANT_LIST_REQUEST });
    const {
      userLogin: { userData },
    } = getState();

    const config = {
      headers: {
        Authorization: `Bearer ${userData.token}`,
      },
    };
    const { data } = await axiosInstance.get("/api/resturant/all", config);

    dispatch({
      type: ALL_RESTURANT_LIST_SUCCESS,
      payload: data,
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message;
    dispatch({
      type: ALL_RESTURANT_LIST_FAIL,
      payload: message,
    });
  }
};
export const createResturantAction =
  (resturantName, foodType, aptName, address, phoneNo, payment) =>
  async (dispatch, getState) => {
    try {
      dispatch({ type: RESTURANT_CREATE_REQUEST });
      const {
        userLogin: { userData },
      } = getState();

      const config = {
        headers: {
          "Content-Type": "application/json",

          Authorization: `Bearer ${userData.token}`,
        },
      };
      const { data } = await axiosInstance.post(
        `/api/resturant/create`,
        {
          resturantName,
          foodType,
          aptName,
          address,
          phoneNo,
          payment,
        },
        config,
      );
      dispatch({
        type: RESTURANT_CREATE_SUCCESS,
        payload: data,
      });
    } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message;
      dispatch({
        type: RESTURANT_CREATE_FAIL,
        payload: message,
      });
    }
  };

export const updateResturantAction =
  (id, resturantName, foodType, aptName, address, phoneNo, payment) =>
  async (dispatch, getState) => {
    try {
      dispatch({
        type: RESTURANT_UPDATE_REQUEST,
      });

      const {
        userLogin: { userData },
      } = getState();

      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${userData.token}`,
        },
      };

      const { data } = await axiosInstance.put(
        `/api/resturant/${id}`,
        { resturantName, foodType, aptName, address, phoneNo, payment },
        config,
      );
      dispatch({
        type: RESTURANT_UPDATE_SUCCESS,
        item: {
          id,
          resturantName,
          foodType,
          aptName,
          address,
          phoneNo,
          payment,
        },
      });
    } catch (error) {
      console.log(error);
      dispatch({
        type: RESTURANT_UPDATE_FAIL,
      });
    }
  };

export const deleteResturantAction = (id) => async (dispatch, getState) => {
  try {
    dispatch({
      type: RESTURANT_DELETE_REQUEST,
    });

    const {
      userLogin: { userData },
    } = getState();

    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${userData.token}`,
      },
    };

    await axiosInstance.delete(`/api/resturant/${id}`, config);
    dispatch({
      type: RESTURANT_DELETE_SUCCESS,
      id,
    });
  } catch (error) {
    coonsole.log(error);
    dispatch({
      type: RESTURANT_DELETE_FAIL,
    });
  }
};
