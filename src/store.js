import { applyMiddleware, combineReducers, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import { authenticationUser } from "./reducers/userreducers";
import { resturantReducer } from "./reducers/resturantreducer";
import { itemReducer } from "./reducers/itemreducer";
import { cardReducer } from "./reducers/cartreducer";

const reducer = combineReducers({
  userLogin: authenticationUser,
  resutrantInfo: resturantReducer,
  itemInfo: itemReducer,
  cartInfo: cardReducer,
});

const middleWare = [thunk];

const store = createStore(
  reducer,
  composeWithDevTools(applyMiddleware(...middleWare))
);

export default store;
