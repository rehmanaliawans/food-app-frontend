import "./App.css";
import ButtonAppBar from "./compenents/AppBar";
import { BrowserRouter, Route, Switch } from "react-router-dom";

//PAGES
import Home from "./Pages/home";
import Login from "./Pages/Login";
import Register from "./Pages/Register";

import AddResturant from "./Pages/addResturant";
import SellerDashboard from "./Pages/SellerDashboard";
import UpdateResturant from "./Pages/updateResturant";
import AddItem from "./Pages/addItems";
import UpdateItem from "./Pages/updateItem";
import ShowItems from "./Pages/showItems";
import Order from "./Pages/order";
import ShowCartItems from "./Pages/cartItems";
import store from "./store";
import { Provider } from "react-redux";
function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <ButtonAppBar />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/Login" component={Login} />
          <Route exact path="/Register" component={Register} />
          <Route exact path="/addRestaurant" component={AddResturant} />
          <Route
            exact
            path="/updateResturant/:id"
            component={UpdateResturant}
          />
          <Route exact path="/Sdashboard" component={SellerDashboard} />
          <Route exact path="/addItems/:id" component={AddItem} />
          <Route exact path="/showItems/:id" component={ShowItems} />
          <Route exact path="/updateItem/:id" component={UpdateItem} />
          <Route exact path="/cartItems/:id" component={ShowCartItems} />
          <Route exact path="/cartItems" component={ShowCartItems} />
          <Route exact path="/order/:id" component={Order} />
        </Switch>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
