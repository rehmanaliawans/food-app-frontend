import {
  ALL_RESTURANT_LIST_FAIL,
  ALL_RESTURANT_LIST_REQUEST,
  ALL_RESTURANT_LIST_SUCCESS,
  RESTURANT_CREATE_FAIL,
  RESTURANT_CREATE_REQUEST,
  RESTURANT_CREATE_SUCCESS,
  RESTURANT_DELETE_FAIL,
  RESTURANT_DELETE_REQUEST,
  RESTURANT_DELETE_SUCCESS,
  RESTURANT_LIST_FAIL,
  RESTURANT_LIST_REQUEST,
  RESTURANT_LIST_SUCCESS,
  RESTURANT_UPDATE_FAIL,
  RESTURANT_UPDATE_REQUEST,
  RESTURANT_UPDATE_SUCCESS,
} from "../constants/resturantConstant";

const initialState = {
  loading: false,
  resturant: [],
};

const DeleteResturant = (state, action) => {
  const rest = state.resturant.filter(
    (resturant) => resturant._id !== action.id
  );
  return {
    ...state,
    resturant: rest,
  };
};

export const resturantReducer = (state = initialState, action) => {
  switch (action.type) {
    case RESTURANT_LIST_REQUEST:
      return { ...state, loading: true };
    case RESTURANT_LIST_SUCCESS:
      return { ...state, loading: false, resturant: action.payload };
    case RESTURANT_LIST_FAIL:
      return { ...state, loading: false, error: action.payload };
    case ALL_RESTURANT_LIST_REQUEST:
      return { ...state, loading: true };
    case ALL_RESTURANT_LIST_SUCCESS:
      return { ...state, loading: false, resturant: action.payload };
    case ALL_RESTURANT_LIST_FAIL:
      return { ...state, loading: false, error: action.payload };
    case RESTURANT_CREATE_REQUEST:
      return { ...state };
    case RESTURANT_CREATE_SUCCESS:
      return { ...state };
    case RESTURANT_CREATE_FAIL:
      return { ...state, error: action.payload };
    case RESTURANT_UPDATE_REQUEST:
      return { ...state };
    case RESTURANT_UPDATE_SUCCESS:
      return {
        ...state,
        resturant: state.resturant.map((resturant) =>
          resturant._id === action.item.id
            ? {
                ...resturant,
                _id: action.item.id,
                resturantName: action.item.resturantName,
                foodType: action.item.foodType,
                aptName: action.item.aptName,
                address: action.item.address,
                phoneNo: action.item.phoneNo,
                payment: action.item.payment,
              }
            : resturant
        ),
      };
    case RESTURANT_UPDATE_FAIL:
      return { ...state };
    case RESTURANT_DELETE_REQUEST:
      return { ...state };
    case RESTURANT_DELETE_SUCCESS:
      return DeleteResturant(state, action);
    case RESTURANT_DELETE_FAIL:
      return { ...state };
    default:
      return state;
  }
};
