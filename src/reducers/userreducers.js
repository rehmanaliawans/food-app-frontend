import {
  USER_LOGIN_FAIL,
  USER_LOGIN_REQUEST,
  USER_LOGIN_SUCCESS,
  USER_LOGOUT,
  USER_REGISTER_FAIL,
  USER_REGISTER_REQUEST,
  USER_REGISTER_SUCCESS,
} from "../constants/userConstant";

const userInfoFromStorage = localStorage.getItem("userData")
  ? JSON.parse(localStorage.getItem("userData"))
  : null;

const initialState = {
  userData: userInfoFromStorage,
  isAuthenticated: userInfoFromStorage ? true : false,
  error: null,
};

export const authenticationUser = (state = initialState, action) => {
  switch (action.type) {
    case USER_LOGIN_REQUEST:
      return { ...state, isAuthenticated: false };
    case USER_LOGIN_SUCCESS:
      return { ...state, isAuthenticated: true, userData: action.payload };
    case USER_LOGIN_FAIL:
      return { ...state, isAuthenticated: false, error: action.payload };
    case USER_LOGOUT:
      return { isAuthenticated: false, userData: null };
    case USER_REGISTER_REQUEST:
      return { ...state };
    case USER_REGISTER_SUCCESS:
      return { ...state, userData: action.payload };
    case USER_REGISTER_FAIL:
      return { ...state, error: action.payload };
    default:
      return state;
  }
};
