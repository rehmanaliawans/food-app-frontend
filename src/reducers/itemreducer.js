import {
  ITEM_CREATE_FAIL,
  ITEM_CREATE_REQUEST,
  ITEM_CREATE_SUCCESS,
  ITEM_DELETE_FAIL,
  ITEM_DELETE_REQUEST,
  ITEM_DELETE_SUCCESS,
  ITEM_LIST_FAIL,
  ITEM_LIST_REQUEST,
  ITEM_LIST_SUCCESS,
  ITEM_UPDATE_FAIL,
  ITEM_UPDATE_REQUEST,
  ITEM_UPDATE_SUCCESS,
} from "../constants/itemConstant";

const initialState = {
  loading: false,
  item: [],
};

export const itemReducer = (state = initialState, action) => {
  switch (action.type) {
    case ITEM_LIST_REQUEST:
      return { ...state, loading: true };
    case ITEM_LIST_SUCCESS:
      return { ...state, loading: false, item: action.payload };
    case ITEM_LIST_FAIL:
      return { ...state, error: action.payload };
    case ITEM_CREATE_REQUEST:
      return { ...state };
    case ITEM_CREATE_SUCCESS:
      return { ...state };
    case ITEM_CREATE_FAIL:
      return { ...state, error: action.payload };
    case ITEM_UPDATE_REQUEST:
      return { ...state };
    case ITEM_UPDATE_SUCCESS:
      return { ...state };
    case ITEM_UPDATE_FAIL:
      return { ...state };
    case ITEM_DELETE_REQUEST:
      return { ...state };
    case ITEM_DELETE_SUCCESS:
      return { ...state };
    case ITEM_DELETE_FAIL:
      return { ...state };
    default:
      return state;
  }
};
