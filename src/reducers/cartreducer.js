import {
  ADD_CART_FAIL,
  ADD_CART_SUCCESS,
  ADD_ORDER_FAIL,
  ADD_ORDER_REQUEST,
  ADD_ORDER_SUCCESS,
  ALL_LIST_ORDER_FAIL,
  ALL_LIST_ORDER_REQUEST,
  ALL_LIST_ORDER_SUCCESS,
  CART_REMOVE,
  LIST_ORDER_FAIL,
  LIST_ORDER_REQUEST,
  LIST_ORDER_SUCCESS,
  ORDER_STATUS_FAIL,
  ORDER_STATUS_REQUEST,
  ORDER_STATUS_SUCCESS,
} from "../constants/cardConstant";

const initialState = {
  cart: [],
  success: false,
  orderData: [],
  status: null,
};

const AddtoCartSuccess = (state, action) => {
  const localcart = state.cart;
  localcart.push(action.data);
  return { ...state, cart: localcart, success: true };
};

const RemoveCart = (state, action) => {
  const rcart = state.cart.filter((item, index) => index !== action.id);

  return {
    ...state,
    cart: rcart,
  };
};
export const cardReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_CART_SUCCESS:
      return AddtoCartSuccess(state, action);
    case ADD_CART_FAIL:
      return { ...state, success: false };
    case CART_REMOVE:
      return RemoveCart(state, action);
    case ADD_ORDER_REQUEST:
      return { ...state };
    case ADD_ORDER_SUCCESS:
      return { ...state, cart: [] };
    case ADD_ORDER_FAIL:
      return { ...state };
    case LIST_ORDER_REQUEST:
      return { ...state, orderData: [] };
    case LIST_ORDER_SUCCESS:
      return { ...state, orderData: action.data };
    case LIST_ORDER_FAIL:
      return { ...state };
    case ALL_LIST_ORDER_REQUEST:
      return { ...state, orderData: [] };
    case ALL_LIST_ORDER_SUCCESS:
      return { ...state, orderData: action.data };
    case ALL_LIST_ORDER_FAIL:
      return { ...state };
    case ORDER_STATUS_REQUEST:
      return { ...state };
    case ORDER_STATUS_SUCCESS:
      return {
        ...state,
        orderData: state.orderData.map((orderData) =>
          orderData._id === action.id
            ? {
                ...orderData,
                status: action.status,
              }
            : orderData
        ),
      };

    case ORDER_STATUS_FAIL:
      return { ...state };
    default:
      return state;
  }
};
