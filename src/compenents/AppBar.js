import React, { Fragment } from "react";
import { AppBar, Button, Toolbar, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { Logout } from "../actions/useraction";

const useStyles = makeStyles(() => ({
  appBar: {
    backgroundColor: "#e8ede1",
    marginBottom: 10,
  },
  title: { flex: 1, color: "black" },
  buttonStyles: {
    color: "black",
    margin: "0 10px 0",
    display: "inline-block",
  },
  buttons: {
    marginRight: 60,
  },
  name: {
    fontStyle: "bold",
    fontSize: 32,
  },
}));
export default function ButtonAppBar() {
  const userLogin = useSelector((state) => state.userLogin);
  const { isAuthenticated, userData } = userLogin;

  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();

  const logoutHandler = () => {
    dispatch(Logout());
    history.push("/Login");
  };
  const sellerLinks = (
    <Fragment>
      <Link to="/SDashboard">
        <Button className={classes.buttonStyles} variant="outlined">
          Resturants
        </Button>
      </Link>
      <Link to="/addRestaurant">
        <Button className={classes.buttonStyles} variant="outlined">
          Add Resturents
        </Button>
      </Link>
      {/* {userData ? (
        <Link to={`/order/${userData._id}`}>
          <Button variant="outlined">Orders List</Button>
        </Link>
      ) : (
        <></>
      )} */}

      <Button
        className={classes.buttonStyles}
        variant="outlined"
        onClick={logoutHandler}
      >
        Logout
      </Button>
    </Fragment>
  );
  const userLinks = (
    <Fragment>
      <Typography variant="h6" noWrap>
        <span className={classes.title}>
          Welcome {userData ? userData.firstname : "no name"}
        </span>
      </Typography>
      <Link to={`/cartItems`}>
        <Button className={classes.buttonStyles} variant="outlined">
          Cart
        </Button>
      </Link>
      <Link to="/SDashboard">
        <Button className={classes.buttonStyles} variant="outlined">
          Resturents
        </Button>
      </Link>
      <Button
        className={classes.buttonStyles}
        variant="outlined"
        onClick={logoutHandler}
      >
        Logout
      </Button>
    </Fragment>
  );
  const loginLinks = (
    <Fragment>
      <Link to="/Login">
        <Button className={classes.buttonStyles} variant="outlined">
          Login
        </Button>
      </Link>
      <Link to="/Register">
        <Button className={classes.buttonStyles} variant="outlined">
          Register
        </Button>
      </Link>
    </Fragment>
  );
  return (
    <AppBar position="static" className={classes.appBar}>
      <Toolbar>
        <Link to="/" className={classes.title}>
          <Typography variant="h6" noWrap>
            <span className={classes.name}>PickUpFood</span>
          </Typography>
        </Link>
        <div className={classes.buttons}>
          {isAuthenticated
            ? userData.isOwner
              ? sellerLinks
              : userLinks
            : loginLinks}
        </div>
      </Toolbar>
    </AppBar>
  );
}
