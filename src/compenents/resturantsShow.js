import React from "react";
import Box from "@mui/material/Box";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import ListItemText from "@mui/material/ListItemText";
import Avatar from "@mui/material/Avatar";
import Grid from "@mui/material/Grid";
import FolderIcon from "@mui/icons-material/Folder";
import { Button, CircularProgress, Divider } from "@material-ui/core";
import { useHistory } from "react-router";
import { useSelector } from "react-redux";

const ResturantsShow = ({ rest }, { loading }) => {
  const cartInfo = useSelector((state) => state.cartInfo);
  const { cart } = cartInfo;
  const history = useHistory();

  const checksingleresturant = (id) => {
    if (cart.length > 0) {
      if (id === cart[0].resturant) {
        history.push(`/showItems/${id}`);
      } else {
        console.log(" do not select any other resturant");
      }
    } else {
      history.push(`/showItems/${id}`);
    }
  };
  return (
    <>
      {loading && <CircularProgress size={30} />}

      <Box key={rest._id}>
        <Grid key={rest._id} item xs={12} md={6}>
          <List>
            <ListItem
              secondaryAction={
                <Button
                  aria-label="order"
                  variant="outlined"
                  onClick={() => checksingleresturant(rest._id)}
                >
                  Order Now
                </Button>
              }
            >
              <ListItemAvatar>
                <Avatar>
                  <FolderIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary={rest.resturantName} />
            </ListItem>
            <Divider light />
          </List>
        </Grid>
      </Box>
    </>
  );
};

export default ResturantsShow;
