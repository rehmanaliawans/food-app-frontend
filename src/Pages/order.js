import { Button, Grid, Paper, TextField, Typography } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  changeOrderStatus,
  listAllOrderAction,
  listOrderAction,
} from "../actions/cardAction";

const Order = ({ match, history }) => {
  const userLogin = useSelector((state) => state.userLogin);
  const { userData } = userLogin;
  const dispatch = useDispatch();

  const cartInfo = useSelector((state) => state.cartInfo);
  const { orderData } = cartInfo;

  useEffect(() => {
    if (userData) {
      if (!userData.isOwner) {
        dispatch(listOrderAction(match.params.id));
      } else {
        dispatch(listAllOrderAction());
      }
    } else {
      history.push("/");
    }
  }, [history, userData]);

  const handleCancel = (orderid) => {
    const status = "Cancelled";
    dispatch(changeOrderStatus(orderid, status));
  };

  const handleAccept = (orderid) => {
    const status = "PROCESSING";
    dispatch(changeOrderStatus(orderid, status));
  };
  const handleRoute = (orderid) => {
    const status = "INROUTE";
    dispatch(changeOrderStatus(orderid, status));
  };
  const handleDelivered = (orderid) => {
    const status = "DELIVERED";
    dispatch(changeOrderStatus(orderid, status));
  };
  const handleReceived = (orderid) => {
    const status = "RECEIVED";
    dispatch(changeOrderStatus(orderid, status));
  };

  return (
    <>
      {orderData &&
        orderData.reverse().map((order) => {
          return (
            <Grid container direction="row" spacing={2} key={order._id}>
              <Grid item sm={1} />
              <Grid item sm={3}>
                <Paper style={{ backgroundColor: "#faf7f7" }} elevation={4}>
                  <Grid
                    style={{
                      marginLeft: 20,
                      marginRight: 20,
                      marginTop: 30,
                      paddingTop: 40,
                      paddingBottom: 40,
                    }}
                  >
                    <Typography variant="body2" color="textPrimary">
                      <span>User Name:</span>
                      <span>{order.firstname}</span>

                      <br />
                      <br />
                      <span>Order Time:</span>
                      <span>{order.createdAt}</span>

                      <br />
                      <br />

                      <span>Total Price</span>
                      <span>Rs. {order.price}</span>

                      <br />
                    </Typography>

                    <hr />
                    <Typography gutterBottom variant="h5" noWrap>
                      <span>Status</span>
                      <span> {order.status}</span>

                      <br />
                    </Typography>

                    {!userData.isOwner && order.status === "PLACED" && (
                      <Button
                        variant="outlined"
                        onClick={() => handleCancel(order._id)}
                        disabled={order.status !== "PLACED"}
                      >
                        Cancel Order
                      </Button>
                    )}
                    {userData.isOwner && order.status === "PLACED" && (
                      <>
                        <Grid style={{ display: "inline-block" }}>
                          <Button
                            variant="outlined"
                            onClick={() => handleCancel(order._id)}
                          >
                            Cancel Order
                          </Button>
                          <Button
                            variant="outlined"
                            onClick={() => handleAccept(order._id)}
                          >
                            Accept Order
                          </Button>
                        </Grid>
                      </>
                    )}
                    {userData.isOwner && order.status === "PROCESSING" && (
                      <Button
                        variant="outlined"
                        onClick={() => handleRoute(order._id)}
                      >
                        In Route
                      </Button>
                    )}
                    {userData.isOwner && order.status === "INROUTE" && (
                      <Button
                        variant="outlined"
                        onClick={() => handleDelivered(order._id)}
                      >
                        Order Completed
                      </Button>
                    )}
                    {!userData.isOwner && order.status === "DELIVERED" && (
                      <Button
                        variant="outlined"
                        onClick={() => handleReceived(order._id)}
                      >
                        Order Received
                      </Button>
                    )}
                  </Grid>
                </Paper>
              </Grid>
              <Grid item sm={1} />
            </Grid>
          );
        })}
    </>
  );
};

export default Order;
