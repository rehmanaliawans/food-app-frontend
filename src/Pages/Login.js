import { Button, Grid, TextField, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { Alert } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { Login } from "../actions/useraction";

const useStyles = makeStyles(() => ({
  title: {
    margin: "10px 0px 10px 0px",
  },
  hamBurger: {
    height: 200,
    width: 240,
  },
}));

const LoginScreen = ({ history }) => {
  const classes = useStyles();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const userLogin = useSelector((state) => state.userLogin);
  const { userData, error } = userLogin;

  useEffect(() => {
    if (userData) {
      history.push("/");
    }
  }, [history, userData]);

  const dispatch = useDispatch();

  const submitHandler = async (e) => {
    e.preventDefault();
    dispatch(Login(email, password));
  };
  return (
    <>
      <Grid container>
        <Grid item sm />
        <Grid item sm style={{ marginBottom: 34 }}>
          <Typography variant="h3" className={classes.title}>
            Login
          </Typography>
          <form noValidate onSubmit={submitHandler}>
            <TextField
              id="email"
              name="email"
              label="Email"
              onChange={(e) => setEmail(e.target.value)}
              value={email}
              fullWidth
            />
            <TextField
              id="password"
              name="password"
              type="password"
              label="Password"
              onChange={(e) => setPassword(e.target.value)}
              value={password}
              fullWidth
            />
            <Button type="submit" variant="contained" color="primary">
              Login
            </Button>
            <br />
            <small>
              don't have an account ? sign up <Link to="/Register">here</Link>
            </small>
          </form>
        </Grid>
        <Grid item sm />
      </Grid>
    </>
  );
};

export default LoginScreen;
