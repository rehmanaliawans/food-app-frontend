import {
  Button,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { Register } from "../actions/useraction";

const RegisterScreen = () => {
  const [email, setEmail] = useState("");
  const [firstname, setFirstName] = useState("");
  const [lastname, setLastName] = useState("");
  const [password, setPassword] = useState("");
  const [isOwner, setIsOwner] = useState(false);
  const [confirmpassword, setConfirmPassword] = useState("");

  const dispatch = useDispatch();
  const history = useHistory();
  const userLogin = useSelector((state) => state.userLogin);
  const { userData } = userLogin;

  useEffect(() => {
    if (userData) {
      if (userData.isOwner) {
        history.push("/addRestaurant");
      } else {
        history.push("/");
      }
    }
  }, [history, userData]);

  const submitHandler = async (e) => {
    e.preventDefault();
    if (
      email !== "" &&
      firstname !== "" &&
      lastname !== "" &&
      password !== "" &&
      confirmpassword !== ""
    ) {
      if (password !== confirmpassword) {
        console.log("Password Do Not Match!");
      } else {
        dispatch(Register(firstname, lastname, email, password, isOwner));
      }
    } else {
      console.log("please enter all fields");
    }
  };

  return (
    <Grid container direction="row" spacing={10}>
      <Grid item sm></Grid>
      <Grid item sm={5}>
        <Typography variant="h3">
          Register{" "}
          <span role="img" aria-label="Pizza Emoji">
            🍕
          </span>
        </Typography>
        <form noValidate onSubmit={submitHandler}>
          <TextField
            id="firstName"
            name="firstName"
            label="FirstName"
            onChange={(e) => setFirstName(e.target.value)}
            value={firstname}
            fullWidth
            required
          />
          <TextField
            id="lastName"
            name="lastName"
            label="LastName"
            onChange={(e) => setLastName(e.target.value)}
            value={lastname}
            fullWidth
            required
          />
          <TextField
            id="email"
            name="email"
            label="Email"
            onChange={(e) => setEmail(e.target.value)}
            value={email}
            fullWidth
            required
          />
          <TextField
            id="password"
            name="password"
            type="password"
            label="Password"
            onChange={(e) => setPassword(e.target.value)}
            value={password}
            fullWidth
            required
          />
          <TextField
            id="confirmPassword"
            name="confirmPassword"
            type="password"
            label="Confirm Password"
            onChange={(e) => setConfirmPassword(e.target.value)}
            value={confirmpassword}
            fullWidth
            required
          />

          <InputLabel id="demo-simple-select-label">Role</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={isOwner}
            label="Is Owner"
            onChange={(e) => setIsOwner(e.target.value)}
            fullWidth
          >
            <MenuItem value={true}>OWNER</MenuItem>
            <MenuItem value={false}>USER</MenuItem>
          </Select>

          <Button type="submit" variant="contained" color="primary">
            Sign-up
          </Button>
          <br />
          <small>
            Already have an account ? Login <Link to="/Login">here</Link>
          </small>
        </form>
      </Grid>

      <Grid item sm></Grid>
    </Grid>
  );
};

export default RegisterScreen;
