import { Button, CircularProgress, Grid, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import React, { Fragment, useEffect } from "react";
import { useHistory } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import {
  deleteResturantAction,
  listAllResturantsAction,
  listResturantAction,
} from "../actions/resturantAction";
import ResturantsShow from "../compenents/resturantsShow";

const useStyles = makeStyles({
  borderBottom: {
    borderBottom: "2px solid #000",
    position: "absolute",
    top: "25.5%",
    left: "6.5%",
    bottom: 0,
    height: "40%",
    width: "44%",
  },
  borderLeft: {
    borderLeft: "2px solid #000",
    position: "absolute",
    top: "25.5%",
    left: "6.5%",
    bottom: 0,
    height: "40%",
  },
  para: {
    fontSize: "x-large",
    marginLeft: "32%",
  },

  buttonStyles: {
    color: "black",
    margin: "30px 0px 0",
    display: "block",
  },
});
const SellerDashboard = () => {
  const classes = useStyles();

  const dispatch = useDispatch();
  const history = useHistory();

  const userLogin = useSelector((state) => state.userLogin);
  const { userData } = userLogin;
  const resutrantInfo = useSelector((state) => state.resutrantInfo);
  const { loading, resturant } = resutrantInfo;

  const deleteHandler = (id) => {
    if (window.confirm("Are You Sure?")) {
      dispatch(deleteResturantAction(id));
    }
  };

  useEffect(() => {
    if (!userData) {
      history.push("/");
    } else {
      if (userData.isOwner) {
        dispatch(listResturantAction());
      } else {
        dispatch(listAllResturantsAction());
      }
    }
  }, [dispatch, history, userData]);

  const ResturantOwner = (rest) => {
    return (
      <Grid key={rest._id} container direction="row">
        <Grid item xs={false} sm={1} />
        <Grid item xs={12} sm={6} style={{ marginTop: 120 }}>
          <Typography
            gutterBottom
            variant="h4"
            component="h2"
            style={{ fontStyle: "bold" }}
          >
            {rest.resturantName}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {rest.foodType}
          </Typography>

          <Typography variant="body2" color="textPrimary">
            {rest.payment}
          </Typography>
          <br />
          <Typography variant="body2" color="textPrimary">
            Address:
            {rest.address}
          </Typography>
          <Typography variant="body2" color="textPrimary">
            Call: +92
            {rest.phoneNo}
          </Typography>
          <Typography variant="body2" color="textPrimary">
            Dine-In Timing: 1pm to 12am
          </Typography>
        </Grid>
        <Grid item xs={12} sm={4} style={{ marginTop: 120 }}>
          <Link to={`/addItems/${rest._id}`}>
            <Button
              className={(classes.buttonStyles, classes.buttonStyles1)}
              variant="outlined"
            >
              ADD Meals
            </Button>
          </Link>
          <Link to={`/showItems/${rest._id}`}>
            <Button
              className={(classes.buttonStyles, classes.buttonStyles1)}
              variant="outlined"
            >
              Show Meals
            </Button>
          </Link>
          <Link to={`/updateResturant/${rest._id}`}>
            <Button className={classes.buttonStyles} variant="outlined">
              Edit Resturant
            </Button>
          </Link>
          <Button
            className={classes.buttonStyles}
            variant="outlined"
            onClick={() => deleteHandler(rest._id)}
          >
            Delete Resturant
          </Button>
        </Grid>

        <Grid item xs={false} sm={1} />
      </Grid>
    );
  };
  return (
    <>
      <Typography variant="h4">RESTURENTS</Typography>
      {userData ? (
        <Link to={`/order/${userData._id}`}>
          <Button variant="outlined">Orders List</Button>
        </Link>
      ) : (
        <></>
      )}

      {loading && <CircularProgress size={30} className={classes.progress} />}
      {resturant ? (
        userData && userData.isOwner ? (
          resturant.map((rest) => {
            return ResturantOwner(rest);
          })
        ) : (
          resturant.map((rest) => {
            return <ResturantsShow rest={rest} loading={loading} />;
          })
        )
      ) : (
        <h1>NO resturant</h1>
      )}
    </>
  );
};

export default SellerDashboard;
