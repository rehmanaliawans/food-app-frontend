import React, { useEffect, useState } from "react";
import { Button, Grid, Paper, TextField, Typography } from "@material-ui/core";
import { updateResturantAction } from "../actions/resturantAction";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import axiosInstance from "../utils/axiosInstance";

const UpdateResturant = ({ match, history }) => {
  const [resturantName, setResturantName] = useState("");
  const [foodType, setFoodType] = useState("");
  const [aptName, setAptName] = useState("");
  const [phoneNo, setPhoneNo] = useState("");
  const [payment, setPayment] = useState("");
  const [address, setAddress] = useState("");
  const [date, setDate] = useState("");
  const dispatch = useDispatch();

  const userLogin = useSelector((state) => state.userLogin);
  const { userData } = userLogin;
  useEffect(() => {
    if (!userData) {
      history.push("/");
    } else {
      if (userData.isOwner) {
        const fetching = async () => {
          const { data } = await axiosInstance.get(
            `/api/resturant/${match.params.id}`,
          );
          setResturantName(data.resturantName);
          setFoodType(data.foodType);
          setAptName(data.aptName);
          setPhoneNo(data.phoneNo);
          setPayment(data.payment);
          setAddress(data.address);
          setDate(data.updatedAt);
        };
        fetching();
      } else {
        history.push("/");
      }
    }
  }, [match.params.id, date]);

  const updateHandler = (e) => {
    e.preventDefault();
    if (
      !resturantName ||
      !foodType ||
      !aptName ||
      !phoneNo ||
      !payment ||
      !address
    ) {
      console.log("not filled all requirments");
    } else {
      dispatch(
        updateResturantAction(
          match.params.id,
          resturantName,
          foodType,
          aptName,
          address,
          phoneNo,
          payment,
        ),
      );
      history.push("/SDashboard");
    }
  };

  return (
    <div>
      <Grid container>
        <Grid item xs={1} />
        <Grid item xs={7}>
          <Paper elevation={2}>
            <Grid container>
              <Grid item sm>
                <Typography variant="h4" style={{ textAlign: "center" }}>
                  Add a Restaurant
                </Typography>

                <form noValidate onSubmit={updateHandler}>
                  <TextField
                    id="resturantName"
                    name="resturantName"
                    label="Restaurant Name"
                    placeholder="Your restaurant name"
                    onChange={(e) => setResturantName(e.target.value)}
                    value={resturantName}
                    fullWidth
                    required
                  />
                  <TextField
                    id="foodType"
                    name="foodType"
                    label="Type of Food"
                    placeholder="Fast Food, Bakery"
                    onChange={(e) => setFoodType(e.target.value)}
                    value={foodType}
                    fullWidth
                    required
                  />

                  <TextField
                    id="aptName"
                    name="aptName"
                    label="Floor/Apartment Name"
                    onChange={(e) => setAptName(e.target.value)}
                    value={aptName}
                    fullWidth
                    required
                  />
                  <TextField
                    id="address"
                    name="address"
                    label="Address"
                    onChange={(e) => setAddress(e.target.value)}
                    value={address}
                    fullWidth
                    required
                  />

                  <TextField
                    id="phoneNo"
                    name="phoneNo"
                    label="Contact Number"
                    type="number"
                    onChange={(e) => setPhoneNo(e.target.value)}
                    value={phoneNo}
                    fullWidth
                    required
                  />
                  <TextField
                    id="payment"
                    name="payment"
                    label="Payment Mode"
                    placeholder="Cash, Online"
                    onChange={(e) => setPayment(e.target.value)}
                    value={payment}
                    fullWidth
                    required
                  />
                  <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    fullWidth
                  >
                    Submit
                  </Button>
                  <br />
                </form>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

export default UpdateResturant;
