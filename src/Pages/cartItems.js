import {
  Button,
  Card,
  CardContent,
  Grid,
  IconButton,
  Paper,
  Tooltip,
  Typography,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import { makeStyles } from "@material-ui/styles";
import axios from "axios";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { addOrder, removeCartItem } from "../actions/cardAction";
import { ADD_ORDER_SUCCESS } from "../constants/cardConstant";
import axiosInstance from "../utils/axiosInstance";
const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
  },
  details: {
    display: "flex",
    flexDirection: "column",
  },
  content: {
    flex: "1 0 auto",
  },
  cover: {
    height: "180",
    width: "60%",
  },
  snackbar: {
    width: "100%",
  },
}));

const ShowCartItems = ({ match, history }) => {
  const classes = useStyles();

  const cartInfo = useSelector((state) => state.cartInfo);
  const { cart, orderData } = cartInfo;
  const userLogin = useSelector((state) => state.userLogin);
  const { userData } = userLogin;
  const dispatch = useDispatch();

  const cartItemshow = (item, index) => {
    return (
      <>
        <Card className={classes.root} variant="outlined">
          <div className={classes.details} key={item._id}>
            <CardContent className={classes.content}>
              <Typography component="h5" variant="h5">
                {item.title}
              </Typography>
              <Typography variant="subtitle1" color="textSecondary">
                price: {item.price}
              </Typography>
            </CardContent>
            <div style={{ textAlign: "center" }}>
              <Tooltip title="Delete" placement="bottom">
                <IconButton onClick={() => removeCart(index)}>
                  <DeleteIcon style={{ color: "#f44336" }} />
                </IconButton>
              </Tooltip>
            </div>
          </div>
        </Card>
      </>
    );
  };

  const totalcount = () => {
    let pricetotal = 0;
    cartInfo.cart.forEach((item) => {
      pricetotal = pricetotal + item.price;
    });
    return pricetotal;
  };
  const removeCart = (index) => {
    dispatch(removeCartItem(index));
  };
  const status = "PLACED";
  const price = totalcount();
  const mealList = [];
  const statushistory = [];

  const mealsdata = () => {
    cart.forEach((singleItem) => {
      mealList.push({
        title: singleItem.title,
        price: singleItem.price,
      });
    });
  };

  const submitOrder = (e) => {
    e.preventDefault();
    if (cart.length > 0) {
      mealsdata();
      statushistory.push({
        updateid: userData._id,
        name: userData.firstname,
        status: status,
        date: new Date().toLocaleString(),
      });

      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${userData.token}`,
        },
      };

      const { order } = axiosInstance.post(
        `/api/order/add/${match.params.id}`,
        {
          firstname: userData.firstname,
          price,
          status,
          meals: mealList,
          resturant: match.params.id,
          user: userData._id,
          history: statushistory,
        },
        config,
      );
      dispatch({ type: ADD_ORDER_SUCCESS });

      history.push(`/order/${userData._id}`);
    } else {
      console.log("add items first");
    }
  };

  useEffect(() => {
    if (!userData) {
      history.push("/");
    } else {
      if (userData.isOwner) {
        history.push("/");
      }
    }
  }, [userData]);
  return (
    <>
      <Grid container direction="row" spacing={2}>
        <Grid item md={7}>
          <Paper>
            {cartInfo.cart ? (
              cartInfo.cart.map((item, index) => {
                return cartItemshow(item, index);
              })
            ) : (
              <h1>No cart info</h1>
            )}
          </Paper>
        </Grid>
        <Grid item md={4}>
          <Paper style={{ backgroundColor: "#faf7f7" }} elevation={4}>
            <div style={{ marginLeft: 20, marginRight: 20 }}>
              <br />
              <hr />
              <Typography gutterBottom variant="h5" noWrap>
                <div>
                  <span>Total </span>
                  <span>Rs. {totalcount()}</span>
                </div>
                <br />
              </Typography>
              <Button
                color="secondary"
                style={{
                  color: "#000",
                  width: "100%",
                  marginBottom: "10%",
                }}
                variant="contained"
                onClick={submitOrder}
              >
                Create Order
              </Button>
            </div>
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};

export default ShowCartItems;
