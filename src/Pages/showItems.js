import React, { useEffect, useState } from "react";
import {
  Button,
  Card,
  CardContent,
  CircularProgress,
  IconButton,
  Snackbar,
  Tooltip,
  Typography,
} from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import { makeStyles } from "@material-ui/styles";
import { useDispatch, useSelector } from "react-redux";
import { addCartItem } from "../actions/cardAction";
import { deleteItemAction, listItemAction } from "../actions/itemAction";
import { Link } from "react-router-dom";
const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
  },
  details: {
    display: "flex",
    flexDirection: "column",
  },
  content: {
    flex: "1 0 auto",
  },
  cover: {
    height: "180",
    width: "60%",
  },
  snackbar: {
    width: "100%",
  },
}));
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
const ShowItems = ({ match, history }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const userLogin = useSelector((state) => state.userLogin);
  const { userData } = userLogin;

  const itemInfo = useSelector((state) => state.itemInfo);
  const { loading } = itemInfo;

  const [openSnackBar, setSnackBar] = useState(false);

  const deleteHandler = (id) => {
    if (window.confirm("Are You Sure?")) {
      dispatch(deleteItemAction(id));
      history.push(`/SDashboard`);
    }
  };

  const handleCloseSnackBar = (event, reason) => {
    if (reason === "clickaway") {
      setSnackBar(false);
      return;
    }

    setSnackBar(false);
  };

  const { success } = useSelector((state) => state.cartInfo);

  const handleSnackBar = (event, reason) => {
    if (success || success == null) setSnackBar(true);
  };

  const handleCart = (itemData) => {
    dispatch(addCartItem(itemData));
  };
  useEffect(() => {
    if (!userData) {
      history.push("/");
    } else {
      dispatch(listItemAction(match.params.id));
    }
  }, [dispatch, history]);

  const returnItemData = (item) => {
    let n = 0;
    return (
      <Card className={classes.root} key={item._id} variant="outlined">
        <div className={classes.details}>
          <CardContent className={classes.content}>
            <Typography component="h5" variant="h5">
              {item.title}
            </Typography>
            <Typography variant="subtitle1" color="textSecondary" noWrap>
              {item.description}
            </Typography>
            <Typography variant="subtitle1" color="textSecondary">
              Rs.{item.price}
            </Typography>
          </CardContent>
          {userData && userData.isOwner ? (
            <div style={{ textAlign: "center" }}>
              <Tooltip title="Edit" placement="bottom">
                <Link to={`/updateItem/${item._id}`}>
                  <IconButton>
                    <EditIcon style={{ color: "green" }} />
                  </IconButton>
                </Link>
              </Tooltip>
              <Tooltip title="Delete" placement="bottom">
                <IconButton onClick={() => deleteHandler(item._id)}>
                  <DeleteIcon style={{ color: "#f44336" }} />
                </IconButton>
              </Tooltip>
            </div>
          ) : (
            <Button
              color="secondary"
              style={{
                color: "#000",
                width: "100%",
                marginLeft: "20%",
                marginBottom: "10%",
              }}
              onClick={() => {
                handleCart(item);
                handleSnackBar();
              }}
              variant="contained"
            >
              Add to Cart
            </Button>
          )}
        </div>
      </Card>
    );
  };
  return (
    <>
      {loading && <CircularProgress size={30} className={classes.progress} />}
      {itemInfo.item &&
        itemInfo.item.map((item) => {
          return returnItemData(item);
        })}
      {userData && !userData.isOwner && (
        <>
          {" "}
          <Link
            to={{
              pathname: `/cartItems/${match.params.id}`,
            }}
          >
            <Button
              color="secondary"
              style={{
                color: "#000",
                width: "100%",
                marginLeft: "20%",
                marginBottom: "10%",
              }}
              variant="contained"
            >
              Open Cart
            </Button>
          </Link>
        </>
      )}
      <div className={classes.snackbar}>
        <Snackbar
          open={openSnackBar}
          autoHideDuration={3600}
          onClose={handleCloseSnackBar}
        >
          <Alert
            onClose={handleCloseSnackBar}
            style={{ backgroundColor: "#157a21" }}
          >
            Item added to cart!
          </Alert>
        </Snackbar>
      </div>
    </>
  );
};

export default ShowItems;
