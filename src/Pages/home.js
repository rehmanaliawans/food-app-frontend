import React from "react";

import cover from "../images/FD_on_time.jpg";
import { Box, Button, Grid, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";

const useStyles = makeStyles(() => ({
  presentation: {
    display: "flex",
    width: "90%",
    margin: "auto",
    minHeight: "80vh",
    alignItems: "center",
  },
  introduction: {
    flex: 1,
    paddingLeft: 60,
    height: "340px",
  },
  safeFood: {
    fontSize: 64,
    fontWeight: 400,
  },
  delivery: {
    color: "#157a21",
    fontSize: 64,
    fontWeight: "bold",
    marginTop: -30,
    marginBottom: 20,
  },
  paragraph: {
    width: 400,
    fontSize: 14.5,
  },
  cover: {
    flex: 1,
    display: "flex",
    justifyContent: "center",
    height: "72vh",
  },
  coverImg: {
    height: "100%",
  },
}));

const Home = () => {
  const classes = useStyles();
  const userLogin = useSelector((state) => state.userLogin);
  const { userData } = userLogin;
  return (
    <Box className={classes.presentation}>
      <Grid container>
        <Grid item md={4} className={classes.introduction}>
          <Typography className={classes.safeFood} noWrap>
            Safe Food
          </Typography>
          <Typography className={classes.delivery} noWrap>
            Delivery
          </Typography>
          <Typography variant="body2" className={classes.paragraph}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut lgna aliqua. Ut enim
            ad aliquip ex ea commodo consequat. Lorem ipsum dolor si
          </Typography>
          {userData ? (
            !userData.isOwner && (
              <Link to="/SDashboard">
                <Button variant="outlined" className={classes.ctaOrder}>
                  ORDER NOW
                </Button>
              </Link>
            )
          ) : (
            <></>
          )}
        </Grid>
        <Grid item md={8} className={classes.cover}>
          <img src={cover} alt="food-delivery" className={classes.coverImg} />
        </Grid>
      </Grid>
    </Box>
  );
};
export default Home;
