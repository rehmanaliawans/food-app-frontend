import { Button, Grid, TextField, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { createItemAction } from "../actions/itemAction";

const useStyles = makeStyles(() => ({
  title: {
    margin: "10px 0px 10px 0px",
  },
  hamBurger: {
    height: 200,
    width: 240,
  },
}));

const AddItem = ({ match, history }) => {
  const classes = useStyles();
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");

  const dispatch = useDispatch();

  const userLogin = useSelector((state) => state.userLogin);
  const { userData } = userLogin;

  const resetHandler = () => {
    setTitle("");
    setDescription("");
    setPrice("");
  };

  useEffect(() => {
    if (userData) {
      if (!userData.isOwner) {
        history.push("/");
      }
    } else {
      history.push("/");
    }
  }, [history, userData]);

  const submitHandler = (e) => {
    e.preventDefault();
    if (!title || !description || !price) {
      console.log("not filled all requirments");
    } else {
      dispatch(createItemAction(match.params.id, title, description, price));

      resetHandler();
      history.push("/SDashboard");
    }
  };
  return (
    <Grid container>
      <Grid item sm />
      <Grid item sm style={{ marginBottom: 34 }}>
        <Typography variant="h3" className={classes.title}>
          Addd New Item
        </Typography>
        <form noValidate onSubmit={submitHandler}>
          <TextField
            id="title"
            name="title"
            label="Title"
            onChange={(e) => setTitle(e.target.value)}
            value={title}
            fullWidth
          />
          <TextField
            id="description"
            name="description"
            multiline
            type="text"
            label="Description"
            rows={6}
            onChange={(e) => setDescription(e.target.value)}
            value={description}
            fullWidth
          />
          <TextField
            id="price"
            name="price"
            type="number"
            label="Price"
            onChange={(e) => setPrice(e.target.value)}
            value={price}
            fullWidth
          />
          <Button type="submit" variant="contained" color="primary">
            Submit
          </Button>
          <Link to="/SDashboard">
            <Button type="cancle" variant="contained" color="primary">
              Cancle
            </Button>
          </Link>
          <br />
        </form>
      </Grid>
      <Grid item sm />
    </Grid>
  );
};

export default AddItem;
